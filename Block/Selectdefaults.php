<?php

namespace Kowal\ImageAltFromAttributes\Block;

use Magento\Backend\Block\Template\Context;

/**
 *
 */
class Selectdefaults extends \Magento\Config\Block\System\Config\Form\Field
{
    /**
     * @param Context $context
     * @param array $data
     */
    public function __construct(
        Context $context,
        array   $data = array()
    )
    {
        parent::__construct($context, $data);
    }

    /**
     * @param \Magento\Framework\Data\Form\Element\AbstractElement $element
     * @return string
     */
    protected function _getElementHtml(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        $html = $element->getElementHtml();
        $value = $element->getData('value');
        $html = '
        <select id="imagealtfromattributes_general_imagealtfromattributes_alttag_defaultvalues" style="height:175px;" data-sorted-values="" multiple>
        <option data-select="0" value="{product_name}">Product Name</option>
        <option data-select="0" value="{product_sku}">Product SKU</option>
        <option data-select="0" value="{category_name}">Category Name</option>
        <option data-select="0" value="{product_price}">Product Price</option>
        <option data-select="0" value="{product_color}">Color</option>
        <option data-select="0" value="{product_manufacturer}">Manufacturer</option>
        <option data-select="0" value="{subcategory_name}">Sub Category Name</option>
        <option data-select="0" value="{product_style}">Product Style General</option>
        <option data-select="0" value="{product_material}">Product Material</option>
        <option data-select="0" value="{product_pattern}">Product Pattern</option>
        <option data-select="0" value="{product_climate}">Product Climate</option>
        <option data-select="0" value="{product_new}">Product New</option>
        <option data-select="0" value="{product_sale}">Product Sale</option>
        <option data-select="0" value="{meta_title}">Meta Title</option>
        </select>
        ';
        return $html;
    }
}
