<?php

namespace Kowal\ImageAltFromAttributes\Block;

use Magento\Backend\Block\Template\Context;

/**
 *
 */
class CmsSelectdefaults extends \Magento\Config\Block\System\Config\Form\Field
{
    /**
     * @param Context $context
     * @param array $data
     */
    public function __construct(
        Context $context,
        array   $data = array()
    )
    {
        parent::__construct($context, $data);
    }

    /**
     * @param \Magento\Framework\Data\Form\Element\AbstractElement $element
     * @return string
     */
    protected function _getElementHtml(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        $html = $element->getElementHtml();
        $value = $element->getData('value');
        $html = '
        <select id="imagealtfromattributes_cms_alt_tag_cms_alttag_defaultvalues" style="height:55px;" data-sorted-values="" multiple>
        <option data-select="0" value="{page_title}">Page Title</option>
        <option data-select="0" value="{content_heading}">Content Heading</option>
        </select>
        ';
        return $html;
    }
}
