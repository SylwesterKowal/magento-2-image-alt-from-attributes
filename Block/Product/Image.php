<?php

namespace Kowal\ImageAltFromAttributes\Block\Product;

class Image extends \Magento\Catalog\Block\Product\Image
{
    protected $_categoryFactory;
    protected $_productRepository;
    protected $alttaghelper;
    protected $_product;
    protected $eavConfig;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        array                                            $data = []
    )
    {
        parent::__construct($context, $data);
        $this->_product = $data['product_id'];
    }

    public function getLabel()
    {

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->eavConfig = $objectManager->get('Magento\Eav\Model\Config');
        $this->_categoryFactory = $objectManager->get('Magento\Catalog\Model\CategoryFactory');
        $this->_productRepository = $objectManager->get('Magento\Catalog\Api\ProductRepositoryInterface');
        $this->alttaghelper = $objectManager->get('Kowal\ImageAltFromAttributes\Helper\Image');

        if ($this->alttaghelper->isEnabledInFrontend()) {
            $alttag = $this->alttaghelper->getAltTagStructure();
            // $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
            $currentproduct = $objectManager->create('Magento\Catalog\Model\Product')->load($this->_product);
            preg_match_all('#\{(.*?)\}#', $alttag, $tag);
            if (!empty($tag[1])) {
                if (in_array("product_name", $tag[1])) {
                    $key = array_search('product_name', $tag[1]);
                    $productName = $currentproduct->getName();
                    $tag[1][$key] = $productName;
                }
                if (in_array("meta_title", $tag[1])) {
                    $key = array_search('meta_title', $tag[1]);
                    $mataTitle = $currentproduct->getMetaTitle();
                    $tag[1][$key] = $mataTitle;
                }
                if (in_array("product_sale", $tag[1])) {
                    $key = array_search('product_sale', $tag[1]);
                    $sale = $currentproduct->getSale();
                    if ($sale) {
                        $tag[1][$key] = "Yes";
                    } else {
                        $tag[1][$key] = "No";
                    }
                }
                if (in_array("product_new", $tag[1])) {
                    $key = array_search('product_new', $tag[1]);
                    $new = $currentproduct->getNew();
                    if ($new) {
                        $tag[1][$key] = "Yes";
                    } else {
                        $tag[1][$key] = "No";
                    }
                }
                if (in_array("product_climate", $tag[1])) {
                    $key = array_search('product_climate', $tag[1]);
                    $climate = $currentproduct->getClimate();
                    $climate = explode(",", $climate);
                    $attribute = $this->eavConfig->getAttribute('catalog_product', 'climate');
                    $options = $attribute->getSource()->getAllOptions();
                    $value = '';
                    foreach ($options as $option) {
                        if (in_array($option['value'], $climate)) {
                            $value = $value . $option['label'] . " ";
                        }
                    }
                    if ($value != "") {
                        $tag[1][$key] = $value;
                    }
                }
                if (in_array("product_pattern", $tag[1])) {
                    $key = array_search('product_pattern', $tag[1]);
                    $pattern = $currentproduct->getPattern();
                    $pattern = explode(",", $pattern);
                    $attribute = $this->eavConfig->getAttribute('catalog_product', 'pattern');
                    $options = $attribute->getSource()->getAllOptions();
                    $value = '';
                    foreach ($options as $option) {
                        if (in_array($option['value'], $pattern)) {
                            $value = $value . $option['label'] . " ";
                        }
                    }
                    if ($value != "") {
                        $tag[1][$key] = $value;
                    }
                }
                if (in_array("product_material", $tag[1])) {
                    $key = array_search('product_material', $tag[1]);
                    $material = $currentproduct->getMaterial();
                    $material = explode(",", $material);
                    $attribute = $this->eavConfig->getAttribute('catalog_product', 'material');
                    $options = $attribute->getSource()->getAllOptions();
                    $value = '';
                    foreach ($options as $option) {
                        if (in_array($option['value'], $material)) {
                            $value = $value . $option['label'] . " ";
                        }
                    }
                    if ($value != "") {
                        $tag[1][$key] = $value;
                    }
                }
                if (in_array("product_style", $tag[1])) {
                    $key = array_search('product_style', $tag[1]);
                    $stylegeneral = $currentproduct->getStyleGeneral();
                    $stylegeneral = explode(",", $stylegeneral);
                    $attribute = $this->eavConfig->getAttribute('catalog_product', 'style_general');
                    $options = $attribute->getSource()->getAllOptions();
                    $value = '';
                    foreach ($options as $option) {
                        if (in_array($option['value'], $stylegeneral)) {
                            $value = $value . $option['label'] . " ";
                        }
                    }
                    if ($value != "") {
                        $tag[1][$key] = $value;
                    }
                }
                if (in_array("category_name", $tag[1])) {
                    $productId = $this->_product;
                    $product = $this->_productRepository->getById($productId);
                    $categoryId = $product->getCategoryIds();
                    if (count($categoryId)) {
                        $category = $this->_categoryFactory->create()->load($categoryId[count($categoryId) - 1]);
                        $categoryName = $category->getName();
                        $key = array_search('category_name', $tag[1]);
                        $tag[1][$key] = $categoryName;
                    } else {
                        $key = array_search('category_name', $tag[1]);
                        $tag[1][$key] = '';
                    }
                }
                if (in_array("subcategory_name", $tag[1])) {
                    $productId = $this->_product;
                    $product = $this->_productRepository->getById($productId);
                    $categoryId = $product->getCategoryIds();
                    if (count($categoryId)) {
                        $category = $this->_categoryFactory->create()->load($categoryId[count($categoryId) - 1]);
                        $categoryName = $category->getParentCategory()->getName();
                        $key = array_search('subcategory_name', $tag[1]);
                        $tag[1][$key] = $categoryName;
                    } else {
                        $key = array_search('subcategory_name', $tag[1]);
                        $tag[1][$key] = '';
                    }
                }
                if (in_array("product_sku", $tag[1])) {
                    $key = array_search('product_sku', $tag[1]);
                    $productSku = $currentproduct->getSKU();
                    $tag[1][$key] = $productSku;
                }
                if (in_array("product_price", $tag[1])) {
                    $key = array_search('product_price', $tag[1]);
                    $productPrice = $currentproduct->getFinalPrice();
                    $tag[1][$key] = strval($productPrice);
                }

                if (in_array("product_size", $tag[1])) {
                    $key = array_search('product_size', $tag[1]);
                    $size = $currentproduct->getAttributeText('size');
                    if (empty($size)) {
                        $tag[1][$key] = '';
                    } else {
                        $tag[1][$key] = $size;
                    }
                }
                if (in_array("product_color", $tag[1])) {
                    $key = array_search('product_color', $tag[1]);
                    $color = $currentproduct->getResource()->getAttribute('color')->getFrontend()->getValue($currentproduct);
                    if (empty($color)) {
                        $tag[1][$key] = '';
                    } else {
                        $tag[1][$key] = $color;
                    }
                }
                if (in_array("product_manufacturer", $tag[1])) {
                    $key = array_search('product_manufacturer', $tag[1]);
                    $manufacturer = $currentproduct->getResource()->getAttribute('manufacturer')->getFrontend()->getValue($currentproduct);
                    if (empty($manufacturer)) {
                        $tag[1][$key] = '';
                    } else {
                        $tag[1][$key] = $manufacturer;
                    }
                }
                $alttagtext = implode(" ", $tag[1]);
                $alttagtext = trim(str_replace("  ", " ", $alttagtext));
                return $alttagtext;
            } else {
                return '';
            }
        } else {
            return parent::getLabel();
        }
    }
}
