<?php
namespace Kowal\ImageAltFromAttributes\Observer\Pages;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\UrlInterface;
use Magento\Cms\Model\PageFactory;

class AddAltTag implements ObserverInterface
{
    /**
     * @var \Kowal\ImageAltFromAttributes\Helper\Image
     */
    protected $_alttagHelper;

    /**
     * @param \Kowal\ImageAltFromAttributes\Helper\Image $alttaghelper
     */
    public function __construct(
        \Kowal\ImageAltFromAttributes\Helper\Image $alttaghelper
    ) {
        $this->_alttagHelper = $alttaghelper;
    }

    /**
     * @param Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer) 
    {
        $event = $observer->getEvent();
        $cmsPage = $event->getPage();
        if($this->_alttagHelper->isEnabledInFrontend()){
            $alttag = '';
            if(!empty($cmsPage->getImagesAltTag()) ){
                $alttag = $cmsPage->getImagesAltTag();
            }else{
                if(!empty($this->_alttagHelper->getCmsAltTagStructure()) ){
                    $alttag = $this->_alttagHelper->getCmsAltTagStructure();
                }
            }

            if(!empty($alttag) ){
                preg_match_all('#\{(.*?)\}#', $alttag, $tag);
                if(!empty($tag[1])){
                    if (in_array("page_title", $tag[1])) {
                        $key = array_search('page_title', $tag[1]);
                        $title = $cmsPage->getTitle();
                        $tag[1][$key]= $title;
                        $alttag = str_replace("{page_title}", $title, $alttag);
                    }

                    if (in_array("content_heading", $tag[1])) {
                        $key = array_search('content_heading', $tag[1]);
                        $heading = $cmsPage->getContentHeading();
                        $tag[1][$key]= $heading;
                        $alttag = str_replace("{content_heading}", $heading, $alttag);
                    }

                    $alttag = trim(str_replace("  ", " ", $alttag));
                    $html  = $cmsPage->getContent();
                    $alttag = '$1'.$alttag.'$2';
                    $pattern ='~(<img.*? alt=")("[^>]*>)~i';
                    $html = preg_replace($pattern, $alttag, $html);
                    $cmsPage->setContent($html);
                }else{
                    $alttag = trim(str_replace("  ", " ", $alttag));
                    $html  = $cmsPage->getContent();
                    $alttag = '$1'.$alttag.'$2';
                    $pattern ='~(<img.*? alt=")("[^>]*>)~i';
                    $html = preg_replace($pattern, $alttag, $html);
                    $cmsPage->setContent($html);
                }
            }
        }
    }
}
