require(['jquery'], function($){
    $(document).on("change", "#imagealtfromattributes_general_imagealtfromattributes_alttag_defaultvalues", function () {
        var menu = this;
        // nothing selected
        if ($(menu).val() == null)
        {
            $.each($(menu).find('option'), function(i)
            {
                $(this).data('select','0');
                $(menu).attr('data-sorted-values', '');
            });
        }
        // at least 1 item selected
        else
        {
            $.each($(menu).find('option'), function(i)
            {
                var vals = $(menu).val().join(' ');
                var opt = $(this).val();
                if (vals.toString().indexOf(opt) > -1)
                {
                    if ($(this).data('select') == 0)
                    {
                        $(menu).attr('data-sorted-values', $(menu).attr('data-sorted-values') + $(this).val() + ' ');
                        $(this).data('select', '1');
                    }
                }
                else
                {
                // most recent deletion
                    if ($(this).data('select') == 1)
                    {
                        var string = $(menu).attr('data-sorted-values').replace(new RegExp(opt, 'g'), "");
                        $(menu).attr('data-sorted-values', string);
                        $(this).data('select','0');
                    }
                }
            });
        }
        var result = $(menu).attr('data-sorted-values').replace(/\s/g, "");
        $('#imagealtfromattributes_general_imagealtfromattributes_alttag_structure').html(result);
    })
    $(document).on("change", "#imagealtfromattributes_cms_alt_tag_cms_alttag_defaultvalues", function () {
        var menu = this;
        // nothing selected
        if ($(menu).val() == null)
        {
            $.each($(menu).find('option'), function(i)
            {
                $(this).data('select','0');
                $(menu).attr('data-sorted-values', '');
            });
        }
        // at least 1 item selected
        else
        {
            $.each($(menu).find('option'), function(i)
            {
                var vals = $(menu).val().join(' ');
                var opt = $(this).val();
                if (vals.toString().indexOf(opt) > -1)
                {
                    if ($(this).data('select') == 0)
                    {
                        $(menu).attr('data-sorted-values', $(menu).attr('data-sorted-values') + $(this).val() + ' ');
                        $(this).data('select', '1');
                    }
                }
                else
                {
                // most recent deletion
                    if ($(this).data('select') == 1)
                    {
                        var string = $(menu).attr('data-sorted-values').replace(new RegExp(opt, 'g'), "");
                        $(menu).attr('data-sorted-values', string);
                        $(this).data('select','0');
                    }
                }
            });
        }
        var result = $(menu).attr('data-sorted-values').replace(/\s/g, "");
        $('#imagealtfromattributes_cms_alt_tag_cms_alttag_structure').html(result);
    })
}); // require end here
