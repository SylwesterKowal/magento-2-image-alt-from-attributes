<?php
namespace Kowal\ImageAltFromAttributes\Helper\Data;
use Magento\Catalog\Api\Data\ProductInterface;

class Plugin
{
    /**
     * @var \Kowal\ImageAltFromAttributes\Helper\Image
     */
    protected $_alttagHelper;
    /**
     * @var \Magento\Catalog\Model\CategoryFactory
     */
    protected $_categoryFactory;
    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $_productRepository;
    /**
     * @var \Magento\Eav\Model\Config
     */
    protected $eavConfig;

    /**
     * @param \Kowal\ImageAltFromAttributes\Helper\Image $alttaghelper
     * @param \Magento\Eav\Model\Config $eavConfig
     * @param \Magento\Catalog\Model\CategoryFactory $categoryFactory
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     */
    public function __construct(
        \Kowal\ImageAltFromAttributes\Helper\Image $alttaghelper,
        \Magento\Eav\Model\Config $eavConfig,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
    ) {
        $this->_alttagHelper = $alttaghelper;
        $this->eavConfig = $eavConfig;
        $this->_categoryFactory = $categoryFactory;
        $this->_productRepository = $productRepository;
    }

    /**
     * @param \Magento\ConfigurableProduct\Helper\Data $data
     * @param $result
     * @param ProductInterface $product
     * @return mixed
     */
    public function afterGetGalleryImages(\Magento\ConfigurableProduct\Helper\Data $data, $result, ProductInterface $product)
    {
        $images = $result;
        if($this->_alttagHelper->isEnabledInFrontend()){
            $alttag = $this->_alttagHelper->getAltTagStructure();
            preg_match_all('#\{(.*?)\}#', $alttag, $tag);
            if(!empty($tag[1])){
                if (in_array("product_name", $tag[1])) {
                    $key = array_search('product_name', $tag[1]);
                    $productName = $product->getName();
                    $tag[1][$key]= $productName;
                }
                
                if (in_array("meta_title", $tag[1])) {
                    $key = array_search('meta_title', $tag[1]);
                    $metaTitle = $product->getMetaTitle();
                    $tag[1][$key]= $metaTitle;
                }

                // Improment work
                if (in_array("product_sale", $tag[1])) {
                    $key = array_search('product_sale', $tag[1]);
                    $sale = $product->getSale();
                    if ($sale) {
                        $tag[1][$key]= "Yes";
                    } else {
                        $tag[1][$key]= "No";
                    }
                }

                if (in_array("product_new", $tag[1])) {
                    $key = array_search('product_new', $tag[1]);
                    $new = $product->getNew();
                    if ($new) {
                        $tag[1][$key]= "Yes";
                    } else {
                        $tag[1][$key]= "No";
                    }
                }

                if (in_array("product_climate", $tag[1])) {
                    $key = array_search('product_climate', $tag[1]);
                    $climate = $product->getClimate();
                    $climate = explode(",", $climate);
                    $attribute = $this->eavConfig->getAttribute('catalog_product', 'climate');
                    $options = $attribute->getSource()->getAllOptions();
                    $value = '';
                    foreach($options as $option) {
                        if(in_array($option['value'], $climate)) {
                            $value = $value.$option['label']." ";
                        }
                    }

                    if ($value != "") {
                        $tag[1][$key]= $value;
                    }
                }

                if (in_array("product_pattern", $tag[1])) {
                    $key = array_search('product_pattern', $tag[1]);
                    $pattern = $product->getPattern();
                    $pattern = explode(",", $pattern);
                    $attribute = $this->eavConfig->getAttribute('catalog_product', 'pattern');
                    $options = $attribute->getSource()->getAllOptions();
                    $value = '';
                    foreach($options as $option) {
                        if(in_array($option['value'], $pattern)) {
                            $value = $value.$option['label']." ";
                        }
                    }

                    if ($value != "") {
                        $tag[1][$key]= $value;
                    }
                }

                if (in_array("product_material", $tag[1])) {
                    $key = array_search('product_material', $tag[1]);
                    $material = $product->getMaterial();
                    $material = explode(",", $material);
                    $attribute = $this->eavConfig->getAttribute('catalog_product', 'material');
                    $options = $attribute->getSource()->getAllOptions();
                    $value = '';
                    foreach($options as $option) {
                        if(in_array($option['value'], $material)) {
                            $value = $value.$option['label']." ";
                        }
                    }

                    if ($value != "") {
                        $tag[1][$key]= $value;
                    }
                }

                if (in_array("product_style", $tag[1])) {
                    $key = array_search('product_style', $tag[1]);
                    $stylegeneral = $product->getStyleGeneral();
                    $stylegeneral = explode(",", $stylegeneral);
                    $attribute = $this->eavConfig->getAttribute('catalog_product', 'style_general');
                    $options = $attribute->getSource()->getAllOptions();
                    $value = '';
                    foreach($options as $option) {
                        if(in_array($option['value'], $stylegeneral)) {
                            $value = $value.$option['label']." ";
                        }
                    }

                    if ($value != "") {
                        $tag[1][$key]= $value;
                    }
                }

                if (in_array("category_name", $tag[1])) {
                    $productId = $product->getId();
                    $product = $this->_productRepository->getById($productId);
                    $categoryId = $product->getCategoryIds();
                    if(count($categoryId)){
                        $category = $this->_categoryFactory->create()->load($categoryId[count($categoryId)-1]);
                        $categoryName = $category->getName();
                        $key = array_search('category_name', $tag[1]);
                        $tag[1][$key]= $categoryName;
                    }else{
                        $key = array_search('category_name', $tag[1]);
                        $tag[1][$key]= '';
                    }
                }

                if (in_array("subcategory_name", $tag[1])) {
                    $productId = $product->getId();
                    $product = $this->_productRepository->getById($productId);
                    $categoryId = $product->getCategoryIds();
                    if(count($categoryId)){
                        $category = $this->_categoryFactory->create()->load($categoryId[count($categoryId)-1]);
                        $categoryName = $category->getParentCategory()->getName();
                        $key = array_search('subcategory_name', $tag[1]);
                        $tag[1][$key]= $categoryName;
                    }else{
                        $key = array_search('subcategory_name', $tag[1]);
                        $tag[1][$key]= '';
                    }
                }

                if (in_array("product_sku", $tag[1])) {
                    $key = array_search('product_sku', $tag[1]);
                    $productSku = $product->getSKU();
                    $tag[1][$key]= $productSku;
                }

                if (in_array("product_price", $tag[1])) {
                    $key = array_search('product_price', $tag[1]);
                    $productPrice = $product->getFinalPrice();
                    $tag[1][$key]= strval($productPrice);
                }

                if (in_array("product_size", $tag[1])) {
                    $key = array_search('product_size', $tag[1]);
                    $size = $product->getAttributeText('size');
                    if(empty($size)){
                        $tag[1][$key]= '';
                    }else{
                        $tag[1][$key]= $size;
                    }
                }


                if (in_array("product_color", $tag[1])) {
                    $key = array_search('product_color', $tag[1]);
                    $color = $product->getResource()->getAttribute('color')->getFrontend()->getValue($product);
                    if(empty($color)){
                        $tag[1][$key]= '';
                    }else{
                        $tag[1][$key]= $color;
                    }
                }

                if (in_array("product_manufacturer", $tag[1])) {
                    $key = array_search('product_manufacturer', $tag[1]);
                    $manufacturer = $product->getResource()->getAttribute('manufacturer')->getFrontend()->getValue($product);
                    if(empty($manufacturer)){
                        $tag[1][$key]= '';
                    }else{
                        $tag[1][$key]= $manufacturer;
                    }
                }

                $alttagtext = implode(" ", $tag[1]);
                $alttagtext = trim(str_replace("  ", " ", $alttagtext));
                foreach ($images as $image) {
                    $image->setLabel($alttagtext);
                }

                return $images;
            }
            else{
                return $result;
            }
        }
        else{
            return $result;
        }
    }
}
