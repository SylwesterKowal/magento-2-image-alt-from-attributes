<?php
namespace Kowal\ImageAltFromAttributes\Helper;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;
class Image extends \Magento\Catalog\Helper\Image
{
    const XML_ALTTAG_ENABLED = 'imagealtfromattributes/general/imagealtfromattributes_mod_enable';
    const XML_ALTTAG_STRUCTURE = 'imagealtfromattributes/general/imagealtfromattributes_alttag_structure';
    const XML_CMS_ALTTAG_STRUCTURE = 'imagealtfromattributes/cms_alt_tag/cms_alttag_structure';
    protected $_categoryFactory;
    protected $_productRepository;
    protected $eavConfig;
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Catalog\Model\Product\ImageFactory $productImageFactory,
        \Magento\Framework\View\Asset\Repository $assetRepo,
        \Magento\Framework\View\ConfigInterface $viewConfig,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Eav\Model\Config $eavConfig,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
    ) {
        parent::__construct($context, $productImageFactory, $assetRepo, $viewConfig);
        $this->_categoryFactory = $categoryFactory;
        $this->eavConfig = $eavConfig;
        $this->_productRepository = $productRepository;
    }
    public function isEnabledInFrontend($store = null)
    {
        $isEnabled = true;
        $enabled =  $this->scopeConfig->getValue(self::XML_ALTTAG_ENABLED, ScopeInterface::SCOPE_STORE);
        if ($enabled == null || $enabled == '0') {
            $isEnabled = false;
        }

        return $isEnabled;
    }
    public function getAltTagStructure()
    {
        $alttag = $this->scopeConfig->getValue(self::XML_ALTTAG_STRUCTURE, ScopeInterface::SCOPE_STORE);
        if (!empty($alttag)){
            return $alttag;
        }else{
            return '';
        }
    }
    public function getCmsAltTagStructure()
    {
        $alttag = $this->scopeConfig->getValue(self::XML_CMS_ALTTAG_STRUCTURE, ScopeInterface::SCOPE_STORE);
        if (!empty($alttag)){
            return $alttag;
        }else{
            return '';
        }
    }
    public function getLabel()
    {
        if($this->isEnabledInFrontend()){
            $alttag = $this->getAltTagStructure();
            preg_match_all('#\{(.*?)\}#', $alttag, $tag);
            if(!empty($tag[1])){
                if (in_array("product_name", $tag[1])) {
                    $key = array_search('product_name', $tag[1]);
                    $productName = $this->_product->getName();
                    $tag[1][$key]= $productName;
                }

                // Improment work
                if (in_array("product_sale", $tag[1])) {
                    $key = array_search('product_sale', $tag[1]);
                    $sale = $this->_product->getSale();
                    if ($sale) {
                        $tag[1][$key]= "Yes";
                    } else {
                        $tag[1][$key]= "No";
                    }
                }

                if (in_array("product_new", $tag[1])) {
                    $key = array_search('product_new', $tag[1]);
                    $new = $this->_product->getNew();
                    if ($new) {
                        $tag[1][$key]= "Yes";
                    } else {
                        $tag[1][$key]= "No";
                    }
                }

                if (in_array("product_climate", $tag[1])) {
                    $key = array_search('product_climate', $tag[1]);
                    $climate = $this->_product->getClimate();
                    $climate = explode(",", $climate);
                    $attribute = $this->eavConfig->getAttribute('catalog_product', 'climate');
                    $options = $attribute->getSource()->getAllOptions();
                    $value = '';
                    foreach($options as $option) {
                        if(in_array($option['value'], $climate)) {
                            $value = $value.$option['label']." ";
                        }
                    }

                    if ($value != "") {
                        $tag[1][$key]= $value;
                    }
                }

                if (in_array("product_pattern", $tag[1])) {
                    $key = array_search('product_pattern', $tag[1]);
                    $pattern = $this->_product->getPattern();
                    $pattern = explode(",", $pattern);
                    $attribute = $this->eavConfig->getAttribute('catalog_product', 'pattern');
                    $options = $attribute->getSource()->getAllOptions();
                    $value = '';
                    foreach($options as $option) {
                        if(in_array($option['value'], $pattern)) {
                            $value = $value.$option['label']." ";
                        }
                    }

                    if ($value != "") {
                        $tag[1][$key]= $value;
                    }
                }

                if (in_array("product_material", $tag[1])) {
                    $key = array_search('product_material', $tag[1]);
                    $material = $this->_product->getMaterial();
                    $material = explode(",", $material);
                    $attribute = $this->eavConfig->getAttribute('catalog_product', 'material');
                    $options = $attribute->getSource()->getAllOptions();
                    $value = '';
                    foreach($options as $option) {
                        if(in_array($option['value'], $material)) {
                            $value = $value.$option['label']." ";
                        }
                    }

                    if ($value != "") {
                        $tag[1][$key]= $value;
                    }
                }

                if (in_array("product_style", $tag[1])) {
                    $key = array_search('product_style', $tag[1]);
                    $stylegeneral = $this->_product->getStyleGeneral();
                    $stylegeneral = explode(",", $stylegeneral);
                    $attribute = $this->eavConfig->getAttribute('catalog_product', 'style_general');
                    $options = $attribute->getSource()->getAllOptions();
                    $value = '';
                    foreach($options as $option) {
                        if(in_array($option['value'], $stylegeneral)) {
                            $value = $value.$option['label']." ";
                        }
                    }

                    if ($value != "") {
                        $tag[1][$key]= $value;
                    }
                }

                // end Improvement work
                if (in_array("category_name", $tag[1])) {
                    $productId = $this->_product->getId();
                    $product = $this->_productRepository->getById($productId);
                    $categoryId = $product->getCategoryIds();
                    if(count($categoryId)){
                        $category = $this->_categoryFactory->create()->load($categoryId[count($categoryId)-1]);
                        $categoryName = $category->getName();
                        $key = array_search('category_name', $tag[1]);
                        $tag[1][$key]= $categoryName;
                    }else{
                        $key = array_search('category_name', $tag[1]);
                        $tag[1][$key]= '';
                    }
                }

                if (in_array("subcategory_name", $tag[1])) {
                    $productId = $this->_product->getId();
                    $product = $this->_productRepository->getById($productId);
                    $categoryId = $product->getCategoryIds();
                    if(count($categoryId)){
                        $category = $this->_categoryFactory->create()->load($categoryId[count($categoryId)-1]);
                        $categoryName = $category->getParentCategory()->getName();
                        $key = array_search('subcategory_name', $tag[1]);
                        $tag[1][$key]= $categoryName;
                    }else{
                        $key = array_search('subcategory_name', $tag[1]);
                        $tag[1][$key]= '';
                    }
                }

                if (in_array("product_sku", $tag[1])) {
                    $key = array_search('product_sku', $tag[1]);
                    $productSku = $this->_product->getSKU();
                    $tag[1][$key]= $productSku;
                }

                if (in_array("product_price", $tag[1])) {
                    $key = array_search('product_price', $tag[1]);
                    $productPrice = $this->_product->getFinalPrice();
                    $tag[1][$key]= strval($productPrice);
                }

                if (in_array("product_size", $tag[1])) {
                    $key = array_search('product_size', $tag[1]);
                    $size = $this->_product->getAttributeText('size');
                    if(empty($size)){
                        $tag[1][$key]= '';
                    }else{
                        $tag[1][$key]= $size;
                    }
                }

                if (in_array("product_color", $tag[1])) {
                    $key = array_search('product_color', $tag[1]);
                    $color = $this->_product->getResource()->getAttribute('color')->getFrontend()->getValue($this->_product);
                    if(empty($color)){
                        $tag[1][$key]= '';
                    }else{
                        $tag[1][$key]= $color;
                    }
                }

                if (in_array("product_manufacturer", $tag[1])) {
                    $key = array_search('product_manufacturer', $tag[1]);
                    $manufacturer = $this->_product->getResource()->getAttribute('manufacturer')->getFrontend()->getValue($this->_product);
                    if(empty($manufacturer)){
                        $tag[1][$key]= '';
                    }else{
                        $tag[1][$key]= $manufacturer;
                    }
                }

                $alttagtext =  implode(" ", $tag[1]);
                $alttagtext = trim(str_replace("  ", " ", $alttagtext));
                return $alttagtext;
            }
            else{
                return '';
            }
        }
        else{
            $label = $this->_product->getData($this->getType() . '_' . 'label');
            if (empty($label)) {
                $label = $this->_product->getName();
            }

            return $label;
        }
    }
}
